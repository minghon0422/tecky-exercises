const cross = '<i class="fas fa-times"></i>'
const circle = '<i class="far fa-circle"></i>'
const boxes = document.querySelectorAll('.box')
const playerTurn = document.querySelector('#playerTurn')
const reset = document.querySelector('.bottom')
const winTurn = document.querySelector('.turn')

let turn = 1;
let currentTurn = cross

for (let box of boxes) {
    box.addEventListener('click', () => {

        if (box.innerHTML == "") {
            if (turn % 2 === 1) {
                box.innerHTML = cross;
                currentTurn = cross;
                playerTurn.innerHTML = circle;
            } else {
                box.innerHTML = circle;
                currentTurn = circle;
                playerTurn.innerHTML = cross;
            }
            checkWin();
            turn++;
        } else {
            return;
        }

    })
}

// Explaining how the result of the array works:
// boxes [
//     [cross/circle],[cross/circle],[cross/circle],[cross/circle],[cross/circle],
// ]

const winningPatterns = [
    [0, 1, 2],
    [0, 3, 6],
    [0, 4, 8],
    [1, 4, 7],
    [2, 5, 8],
    [3, 4, 5],
    [6, 7, 8],
    [6, 4, 2]
]

function checkWin() {
    for (let winningPattern of winningPatterns) {
        let [ a, b, c]= winningPattern
        if (boxes[a].innerHTML == currentTurn && boxes[b].innerHTML == currentTurn && boxes[c].innerHTML == currentTurn) {
            winTurn.innerHTML = `${currentTurn} win`;
        }
        continue;

    }
    return;
}

reset.addEventListener('click', () => {
    for (let box of boxes) {
        box.innerHTML = ''
    }
    winTurn.innerHTML = '<i class="fas fa-times"></i> Turn';
})
