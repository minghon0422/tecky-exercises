const unitLength  = 15;
let boxColor    = '#ff0000';
let strokeColor = '#f7ed2d';
let backgroundColor = '#ffffff';
let columns; /* To be determined by window width */
let rows;    /* To be determined by window height */
let currentBoard;
let nextBoard;
let play = true;
let a = 2;
let b = 3;
let c = 3;

// let audio = new Audio('/Users/tycho/Desktop/tecky-exercises/Flo Rida - Low ft. T-Pain.wav');
let low = document.getElementById("low");
const slider = document.querySelector('.slider');

function setup(){
    /* Set the canvas to be under the element #canvas*/
    const canvas = createCanvas(windowWidth -100, windowHeight - 100);
    canvas.parent(document.querySelector('#canvas'));

    /*Calculate the number of columns and rows */
    columns = floor(width  / unitLength);
    rows    = floor(height / unitLength);

    /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    // Now both currentBoard and nextBoard are array of array of undefined values.
    begin();  // Set the initial values of the currentBoard and nextBoard
}


function  begin() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
}
/**
* Initialize/reset the board state
*/
function  init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = random() > 0.8 ? 1 : 0;
            nextBoard[i][j] = 0;
        }
    }
}


function draw() {
    
    background(255);
    frameRate(parseInt(slider.value));
    if (play){
        generate();
    }
    
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {

            let r = 235, g = 200, b = 155

            r = r * 1.5 * (i / columns)
            g = g * 1 * (j / rows)
            // b = b * 2 * (i * J / rows / columns)

            if (currentBoard[i][j] == 1){
                fill(r,g,b);  
            } else {
                fill(backgroundColor);
            } 
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
}


function generate() {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if( i == 0 && j == 0 ){
                        // the cell itself is not its own neighbor
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                }
            }

            // Rules of Life
            if (currentBoard[x][y] == 1 && neighbors < a) {
                // Die of Loneliness
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 1 && neighbors > b) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == c) {
                // New life due to Reproduction
                nextBoard[x][y] = 1;
            } else {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}


/**
 * When mouse is dragged
 */
 function mouseDragged() {
    /**
     * If the mouse coordinate is outside the board
     */
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill(boxColor);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

/**
 * When mouse is pressed
 */
function mousePressed() {
    noLoop();
    mouseDragged();
}

/**
 * When mouse is released
 */
function mouseReleased() {
    play = false;
    loop();
}

function  reset() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
}

function doubleClicked() {
    noLoop();
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 0; 
};

function whiteOrNot() {
    if (backgroundColor === '#ffffff') {
        backgroundColor = "#000000";
        strokeColor = "#2e2d2c";
        playAudio();
    } else { 
    backgroundColor = '#ffffff';
    strokeColor = '#f7ed2d';
    pauseAudio();
}};

function playAudio() {
    low.play();
    low.loop = true;
  }

function pauseAudio() { 
    low.pause(); 
  } 

document.querySelector('#reset-game')
    .addEventListener('click', function() {
        reset();
    });

document.querySelector('#game')
    .addEventListener('click', function() {
        patterns();
        play = true;
        loop();
    });   

document.querySelector('#lv1')
    .addEventListener('click', function() {
        patterns();
        lv1();
        play = true;
        loop();
    });   

document.querySelector('#lv2')
    .addEventListener('click', function() {
        patterns();
        lv2();
        play = true;
        loop();
    });   

document.querySelector('#lv3')
    .addEventListener('click', function() {
        patterns();
        lv3();
        play = true;
        loop();
    });   


document.querySelector('#start')
    .addEventListener('click', function() {
        play = true;
        loop();
    });

document.querySelector('#pause')
    .addEventListener('click', function() {
        play = false;
        noLoop();
    });

document.querySelector('#life-reborn')
    .addEventListener('click', function() {
        init();
        play = true;
        loop();
    });

document.querySelector('#change-style')
    .addEventListener('click', function() {
        strokeColor = color(random() * 255, 200, random() * 255);
        play = true;
        loop();
    });

document.querySelector('#change-mode')
    .addEventListener('click', function() {
       whiteOrNot();
       play = true;
       loop();
    });

    function windowResized() {
        setup();
    }

    console.log(parseInt(slider.value))


// below are testing ground...

function patterns() {
    reset();
    // eye1
    currentBoard[32][11] = 1;
    currentBoard[32][12] = 1;
    currentBoard[32][13] = 1;
    currentBoard[33][12] = 1;
    currentBoard[35][12] = 1;
    currentBoard[36][11] = 1;
    currentBoard[36][12] = 1;
    currentBoard[36][13] = 1;
    // // eye2
    currentBoard[49][11] = 1;
    currentBoard[49][12] = 1;
    currentBoard[49][13] = 1;
    currentBoard[50][12] = 1;
    currentBoard[52][12] = 1;
    currentBoard[53][11] = 1;
    currentBoard[53][12] = 1;
    currentBoard[53][13] = 1;
    // // the mouth
    currentBoard[41][21] = 1;
    currentBoard[41][22] = 1;
    currentBoard[41][23] = 1;
    currentBoard[41][25] = 1;
    currentBoard[40][24] = 1;
    currentBoard[39][22] = 1;
    currentBoard[44][21] = 1;
    currentBoard[44][22] = 1;
    currentBoard[44][23] = 1;
    currentBoard[44][25] = 1;
    currentBoard[45][24] = 1;
    currentBoard[46][22] = 1;
    currentBoard[42][24] = 1;
    currentBoard[43][24] = 1;
}

function lv1() {
    patterns();
    
    // the left birds
    currentBoard[19][3] = 1;
    currentBoard[19][4] = 1;
    currentBoard[19][5] = 1;
    currentBoard[19][6] = 1;
    currentBoard[19][7] = 1;
    currentBoard[19][8] = 1;
    currentBoard[20][9] = 1;
    currentBoard[22][9] = 1;
    currentBoard[20][3] = 1;
    currentBoard[21][3] = 1;
    currentBoard[22][4] = 1;
    currentBoard[23][6] = 1;
    currentBoard[23][7] = 1;
    
    // the right birds
    currentBoard[66][3] = 1;
    currentBoard[66][4] = 1;
    currentBoard[66][5] = 1;
    currentBoard[66][6] = 1;
    currentBoard[66][7] = 1;
    currentBoard[66][8] = 1;
    currentBoard[65][9] = 1;
    currentBoard[63][9] = 1;
    currentBoard[65][3] = 1;
    currentBoard[64][3] = 1;
    currentBoard[63][4] = 1;
    currentBoard[62][6] = 1;
    currentBoard[62][7] = 1;

}

function lv2() {
    patterns();
    
    // the left birds
    currentBoard[19][3] = 1;
    currentBoard[19][4] = 1;
    currentBoard[19][5] = 1;
    currentBoard[19][6] = 1;
    currentBoard[19][7] = 1;
    currentBoard[19][8] = 1;
    currentBoard[20][9] = 1;
    currentBoard[22][9] = 1;
    currentBoard[20][3] = 1;
    currentBoard[21][3] = 1;
    currentBoard[22][4] = 1;
    currentBoard[23][6] = 1;
    currentBoard[23][7] = 1;

    currentBoard[19][27] = 1;
    currentBoard[19][28] = 1;
    currentBoard[19][29] = 1;
    currentBoard[19][30] = 1;
    currentBoard[19][31] = 1;
    currentBoard[19][32] = 1;
    currentBoard[20][33] = 1;
    currentBoard[22][33] = 1;
    currentBoard[20][27] = 1;
    currentBoard[21][27] = 1;
    currentBoard[22][28] = 1;
    currentBoard[23][30] = 1;
    currentBoard[23][31] = 1;
    
    // the right birds
    currentBoard[66][3] = 1;
    currentBoard[66][4] = 1;
    currentBoard[66][5] = 1;
    currentBoard[66][6] = 1;
    currentBoard[66][7] = 1;
    currentBoard[66][8] = 1;
    currentBoard[65][9] = 1;
    currentBoard[63][9] = 1;
    currentBoard[65][3] = 1;
    currentBoard[64][3] = 1;
    currentBoard[63][4] = 1;
    currentBoard[62][6] = 1;
    currentBoard[62][7] = 1;
    
    currentBoard[66][27] = 1;
    currentBoard[66][28] = 1;
    currentBoard[66][29] = 1;
    currentBoard[66][30] = 1;
    currentBoard[66][31] = 1;
    currentBoard[66][32] = 1;
    currentBoard[65][33] = 1;
    currentBoard[63][33] = 1;
    currentBoard[65][27] = 1;
    currentBoard[64][27] = 1;
    currentBoard[63][28] = 1;
    currentBoard[62][30] = 1;
    currentBoard[62][31] = 1;

}

function lv3() {
    patterns();
    
    // the left birds
    currentBoard[19][3] = 1;
    currentBoard[19][4] = 1;
    currentBoard[19][5] = 1;
    currentBoard[19][6] = 1;
    currentBoard[19][7] = 1;
    currentBoard[19][8] = 1;
    currentBoard[20][9] = 1;
    currentBoard[22][9] = 1;
    currentBoard[20][3] = 1;
    currentBoard[21][3] = 1;
    currentBoard[22][4] = 1;
    currentBoard[23][6] = 1;
    currentBoard[23][7] = 1;

    currentBoard[19][15] = 1;
    currentBoard[19][16] = 1;
    currentBoard[19][17] = 1;
    currentBoard[19][18] = 1;
    currentBoard[19][19] = 1;
    currentBoard[19][20] = 1;
    currentBoard[20][21] = 1;
    currentBoard[22][21] = 1;
    currentBoard[20][15] = 1;
    currentBoard[21][15] = 1;
    currentBoard[22][16] = 1;
    currentBoard[23][18] = 1;
    currentBoard[23][19] = 1;

    currentBoard[19][27] = 1;
    currentBoard[19][28] = 1;
    currentBoard[19][29] = 1;
    currentBoard[19][30] = 1;
    currentBoard[19][31] = 1;
    currentBoard[19][32] = 1;
    currentBoard[20][33] = 1;
    currentBoard[22][33] = 1;
    currentBoard[20][27] = 1;
    currentBoard[21][27] = 1;
    currentBoard[22][28] = 1;
    currentBoard[23][30] = 1;
    currentBoard[23][31] = 1;
    
    // the right birds
    currentBoard[66][3] = 1;
    currentBoard[66][4] = 1;
    currentBoard[66][5] = 1;
    currentBoard[66][6] = 1;
    currentBoard[66][7] = 1;
    currentBoard[66][8] = 1;
    currentBoard[65][9] = 1;
    currentBoard[63][9] = 1;
    currentBoard[65][3] = 1;
    currentBoard[64][3] = 1;
    currentBoard[63][4] = 1;
    currentBoard[62][6] = 1;
    currentBoard[62][7] = 1;

    currentBoard[66][15] = 1;
    currentBoard[66][16] = 1;
    currentBoard[66][17] = 1;
    currentBoard[66][18] = 1;
    currentBoard[66][19] = 1;
    currentBoard[66][20] = 1;
    currentBoard[65][21] = 1;
    currentBoard[63][21] = 1;
    currentBoard[65][15] = 1;
    currentBoard[64][15] = 1;
    currentBoard[63][16] = 1;
    currentBoard[62][18] = 1;
    currentBoard[62][19] = 1;
    
    currentBoard[66][27] = 1;
    currentBoard[66][28] = 1;
    currentBoard[66][29] = 1;
    currentBoard[66][30] = 1;
    currentBoard[66][31] = 1;
    currentBoard[66][32] = 1;
    currentBoard[65][33] = 1;
    currentBoard[63][33] = 1;
    currentBoard[65][27] = 1;
    currentBoard[64][27] = 1;
    currentBoard[63][28] = 1;
    currentBoard[62][30] = 1;
    currentBoard[62][31] = 1;
}

function keyPressed() {
    if (keyCode === LEFT_ARROW) {
        currentBoard[1][33] = 1;
        currentBoard[2][32] = 1;
        currentBoard[3][32] = 1;
        currentBoard[3][33] = 1;
        currentBoard[3][34] = 1;
        
    } else if (keyCode === RIGHT_ARROW) {
        currentBoard[87][33] = 1;
        currentBoard[86][32] = 1;
        currentBoard[85][32] = 1;
        currentBoard[85][33] = 1;
        currentBoard[85][34] = 1;
    }
  }

function visitPage(){
    window.location='/Users/tycho/Desktop/tecky-exercises/LendingPage.html';
}

// document.querySelector('#Loneliness')
//     .addEventListener('input', function(e) {
//         parstinput.value;
//         console.log(a);
//     });

// document.querySelector('#Overpopulation');
// document.querySelector('#Reproduction');

// function generateNew() {
//     //Loop over every single box on the board
//     for (let x = 0; x < columns; x++) {
//         for (let y = 0; y < rows; y++) {
//             // Count all living members in the Moore neighborhood(8 boxes surrounding)
//             let neighbors = 0;
//             for (let i of [-1, 0, 1]) {
//                 for (let j of [-1, 0, 1]) {
//                     if( i == 0 && j == 0 ){
//                         // the cell itself is not its own neighbor
//                         continue;
//                     }
//                     // The modulo operator is crucial for wrapping on the edge
//                     neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
//                 }
//             }

//             // Rules of Life
//             if (currentBoard[x][y] == 1 && neighbors < a) {
//                 // Die of Loneliness
//                 nextBoard[x][y] = 0;
//             } else if (currentBoard[x][y] == 1 && neighbors > b) {
//                 // Die of Overpopulation
//                 nextBoard[x][y] = 0;
//             } else if (currentBoard[x][y] == 0 && neighbors == c) {
//                 // New life due to Reproduction
//                 nextBoard[x][y] = 1;
//             } else {
//                 // Stasis
//                 nextBoard[x][y] = currentBoard[x][y];
//             }
//         }
//     }

//     // Swap the nextBoard to be the current Board
//     [currentBoard, nextBoard] = [nextBoard, currentBoard];
// }

// document.querySelector('#change-rule')
//     .addEventListener('click', function() {
//        clicks();
//        play = true;
//        loop();
//     });

// let clickCount = 0;

// function clicks() {
//     // We modify clickCount variable here to check how many clicks there was
//         clickCount++;
//             if(clickCount == 1) {
//                 generateNew();
//                 console.log("new rule");
//                 console.log (clickCount)
//             } else {
//                 generate()
//                 console.log("original rule");
//             }
//             clickCount = 0;
//         }